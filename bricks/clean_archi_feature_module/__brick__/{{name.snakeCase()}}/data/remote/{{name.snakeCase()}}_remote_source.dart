import 'package:graphql/client.dart';
import 'package:flutter/services.dart';

class {{#pascalCase}}{{name}}{{/pascalCase}}RemoteSource {
  final GraphQLClient _client;

  {{#pascalCase}}{{name}}{{/pascalCase}}RemoteSource(this._client);
}