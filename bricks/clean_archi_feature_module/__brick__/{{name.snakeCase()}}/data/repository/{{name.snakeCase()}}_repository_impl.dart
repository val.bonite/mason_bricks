import '../../domain/repository/{{name.snakeCase()}}_repository.dart';
import '../remote/{{name.snakeCase()}}_remote_source.dart';

class {{#pascalCase}}{{name}}{{/pascalCase}}RepositoryImpl extends {{#pascalCase}}{{name}}{{/pascalCase}}Repository {
  final {{#pascalCase}}{{name}}{{/pascalCase}}RemoteSource _{{#camelCase}}{{name}}{{/camelCase}}RemoteSource;

  {{#pascalCase}}{{name}}{{/pascalCase}}RepositoryImpl(this._{{#camelCase}}{{name}}{{/camelCase}}RemoteSource);

  @override
  Future<void> foo() {
    // TODO: implement foo
    throw UnimplementedError();
  }
}