import 'package:splurge/common/di/service_locator.dart';
import '../data/remote/{{name.snakeCase()}}_remote_source.dart';
import '../data/repository/{{name.snakeCase()}}_repository_impl.dart';

final {{#camelCase}}{{name}}{{/camelCase}}RemoteSource = {{#pascalCase}}{{name}}{{/pascalCase}}RemoteSource(graphQLClient);
final {{#camelCase}}{{name}}{{/camelCase}}Repository = {{#pascalCase}}{{name}}{{/pascalCase}}RepositoryImpl({{#camelCase}}{{name}}{{/camelCase}}RemoteSource);
